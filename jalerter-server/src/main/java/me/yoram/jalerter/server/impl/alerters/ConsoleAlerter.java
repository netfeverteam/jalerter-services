/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.impl.alerters;

import me.yoram.jalerter.server.api.AlertInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 12/08/18
 */
public class ConsoleAlerter extends AbstractAlerter {
    private static final Logger LOG = LogManager.getLogger(ConsoleAlerter.class);

    @Override
    public void alert(final AlertInfo info) {
        LOG.info(String.format(
                "[%s] %s; %s; %s; %s",
                appId,
                info.getUid() == null ? "" : info.getUid(),
                info.getDescription() == null ? "" : info.getDescription(),
                info.getMime() == null ? "" : info.getMime(),
                info.getData() == null ? "" : info.getData()));
    }

    @Override
    public void done() {
        LOG.info("[" + appId + "] alert done!");
    }
}
