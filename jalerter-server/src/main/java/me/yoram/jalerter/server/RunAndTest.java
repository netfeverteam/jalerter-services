/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server;

import me.yoram.jalerter.server.api.AlertInfo;
import me.yoram.jalerter.server.utils.AlerterServiceClient;
import me.yoram.jalerter.server.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Base64;
import java.util.UUID;

/**
 * @author theyoz
 * @since 13/08/18
 */
public class RunAndTest {
    public static void main(String[] args)  {
        if (args.length == 0) {
            return;
        }

        Throwable err = null;
        Process p = null;
        String appid = null;
        String cmd = null;
        var commit = false;

        try {
            for (final var s: args) {
                final var parts = s.split("=");

                if (parts.length != 2) {
                    throw new Exception("parameter must be key=value");
                }

                if ("appid".equals(parts[0])) {
                    appid = parts[1];
                } else if ("cmd".equals(parts[0])) {
                    cmd = parts[1];
                } else {
                    throw new Exception(String.format("Parameter %s is unknown.", parts[0]));
                }
            }

            if (appid == null) {
                appid = UUID.randomUUID().toString();
                commit = true;
            }

            if (cmd == null) {
                throw new Exception("Command not found");
            }

            p = Runtime.getRuntime().exec(cmd);

            while (p.isAlive()) {
                Utils.sleep(500);
            }
        } catch (Throwable t) {
            err = t;

            t.printStackTrace();
        }

        if (p != null && p.exitValue() != 0) {
            AlerterServiceClient.add(
                    appid,
                    new AlertInfo()
                            .uid("Process Output")
                            .description(cmd)
                            .data(
                                    String.format(
                                            "StdOut: %s\n\nErrOut: %s\n\nExit Value: %d",
                                            Utils.toString(p.getInputStream()),
                                            Utils.toString(p.getErrorStream()),
                                            p.exitValue()))
                            .mime("text/plain"));
        }

        if (err != null) {
            final var baos = new ByteArrayOutputStream();
            final var ps = new PrintStream(baos);
            err.printStackTrace(ps);

            AlerterServiceClient.add(
                    appid,
                    new AlertInfo()
                            .uid("Stack Trace")
                            .description(cmd)
                            .data(Base64.getEncoder().encodeToString(baos.toByteArray()))
                            .mime("application/octet"));
        }

        if (commit) {
            AlerterServiceClient.done(appid);
        }
    }
}
