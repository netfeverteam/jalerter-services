/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.utils;

import org.apache.logging.log4j.Logger;

/**
 * @author theyoz
 * @since 12/08/18
 */
public class CheckUtils {
    public static void checkBoolean(
            final boolean b,
            final String errorMessage,
            final Logger log,
            final RuntimeException e) throws Exception {
        if (!b) {
            if (log != null) {
                log.error(errorMessage);
            }

            if (e == null) {
                throw new Exception(errorMessage);
            } else {
                throw e;
            }
        }
    }

    public static void checkBoolean(
            final boolean b,
            final String errorMessage,
            final Logger log) throws Exception {
        checkBoolean(b, errorMessage, log, null);
    }

    public static void checkBoolean(final boolean b, final String errorMessage) throws Exception {
        checkBoolean(b, errorMessage, null);
    }

    public static void checkBooleanSilent(final boolean b, final String errorMessage, final Logger log) {
        if (!b) {
            if (log != null) {
                log.error(errorMessage);
            }

            throw new RuntimeException(errorMessage);
        }
    }
}
