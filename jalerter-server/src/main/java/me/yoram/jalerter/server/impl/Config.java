/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.impl;

import me.yoram.jalerter.server.api.IAlerterFactory;
import me.yoram.jalerter.server.api.IConfig;

import java.util.List;

/**
 * @author theyoz
 * @since 12/08/18
 */
public class Config implements IConfig {
    private static IConfig instance;

    public static IConfig getInstance() {
        return instance;
    }

    public static void setInstance(final IConfig instance) {
        Config.instance = instance;
    }

    private int serviceTcpPort = 8989;
    private List<IAlerterFactory> alerterFactories;

    @Override
    public List<IAlerterFactory> getAlerterFactories() {
        return alerterFactories;
    }

    @Override
    public void setAlerterFactories(final List<IAlerterFactory> alerterFactories) {
        this.alerterFactories = alerterFactories;
    }

    @Override
    public int getServiceTcpPort() {
        return serviceTcpPort;
    }

    @Override
    public void setServiceTcpPort(int serviceTcpPort) {
        this.serviceTcpPort = serviceTcpPort;
    }
}
