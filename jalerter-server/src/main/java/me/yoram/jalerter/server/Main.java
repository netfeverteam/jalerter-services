/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server;

import me.yoram.jalerter.server.api.IAlerterFactory;
import me.yoram.jalerter.server.api.IConfig;
import me.yoram.jalerter.server.impl.Config;
import me.yoram.jalerter.server.utils.CheckUtils;
import me.yoram.jalerter.server.utils.Utils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;


import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

/**
 * @author theyoz
 * @since 10/08/18
 */
public class Main {
    private static Logger LOG = null;

    private static String ENV_ALERTER_HOME = "ALERTER_HOME";

    private static File alerterHome;
    private static ApplicationContext context;

    static {
        try {
            alerterHome = Utils.getAlerterHome();

            var f = Utils.getLog4j2File(alerterHome);

            if (f != null) {
                try {
                    ConfigurationSource source = new ConfigurationSource(new FileInputStream(f));
                    Configurator.initialize(null, source);

                    LOG = LogManager.getLogger(Main.class);
                } catch (Throwable t) {
                    System.err.println(t.getMessage());
                    t.printStackTrace();
                }
            }

            context = configureContext();

            final var conf = getObject(context, "config", IConfig.class);

            if (conf == null) {
                throw new Exception("No config bean defined");
            }

            for (IAlerterFactory factory: conf.getAlerterFactories()) {
                factory.validate();
            }

            Config.setInstance(conf);
        } catch (Throwable t) {
            if (LOG == null) {
                System.err.println(t.getMessage());
                t.printStackTrace();
            } else {
                LOG.error(t.getMessage(), t);
            }

            System.exit(1);
        }
    }

    private static ApplicationContext configureContext() throws Exception {
        var f = Utils.getConfigFile(alerterHome);

        if (f == null) {
            throw new Exception("Couldn't find conf/config.xml file");
        }

        final var res = new FileSystemXmlApplicationContext(f.toURI().toString());

        check(res, "config", IConfig.class, f.getParentFile());

        return res;
    }

    private static <T> T getObject(final ApplicationContext context, final String id, final Class<T> clazz) {
        try {
            return context.getBean(id, clazz);
        } catch(NoSuchBeanDefinitionException e ) {
            return null;
        } catch (BeanNotOfRequiredTypeException e) {
            LOG.error(e.getMessage(), e);
            Object o = context.getBean(id);
            CheckUtils.checkBooleanSilent(
                    false,
                    String.format(
                            "The bean with requested name [%s] exists but is of class name [%s] and cannot be typecast as [%s]. %s",
                            id,
                            o.getClass().getName(),
                            clazz.getName(),
                            e.getMessage()),
                    LOG);
        } catch (BeansException e) {
            LOG.error(e.getMessage(), e);

            CheckUtils.checkBooleanSilent(
                    false,
                    String.format(
                            "The bean with requested name [%s] exists but cannot be instantiated. %s",
                            id,
                            e.getMessage()),
                    LOG);
        }

        return null;
    }

    private static void check(
            final ApplicationContext context,
            final String objectName,
            final Class<?> clazz,
            File configFolder) throws Exception {
        final Object o = getObject(context, objectName, clazz);

        CheckUtils.checkBooleanSilent(
                o != null,
                String.format(
                        "object with id [%s] implementing [%s] has not been found in the config directory [%s].",
                        objectName,
                        clazz.getName(),
                        configFolder.getAbsolutePath()),
                LOG);

        if (o instanceof IAlerterFactory) {
            ((IAlerterFactory)o).validate();

        }
        LOG.info(String.format("%s: %s", objectName, o));
    }

    public static void main(String[] args) throws Exception {
        final ResourceConfig rc = new ResourceConfig();
        rc.register(AlertService.class);
        final HttpServer grizzlyServer = GrizzlyHttpServerFactory.createHttpServer(
                new URI(String.format("http://0.0.0.0:%d/", Config.getInstance().getServiceTcpPort())),
                rc,
                true);
    }
}
