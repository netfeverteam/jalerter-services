/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server;

import me.yoram.jalerter.server.api.AlertInfo;
import me.yoram.jalerter.server.api.IAlerter;
import me.yoram.jalerter.server.impl.Config;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author theyoz
 * @since 10/08/18
 */
@Path("/")
public class AlertService {
    private static final Map<String, List<IAlerter>> ALERTERS = new HashMap<>();
    private static final Object ALERTERS_LOCK = new Object();

    @Path("/alert/{appid}/add")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response alert(
            @PathParam ("appid") final String appid, final AlertInfo info)  {
        CompletableFuture.supplyAsync(() -> {
            final List<IAlerter> alerters;

            synchronized (ALERTERS_LOCK) {
                if (ALERTERS.containsKey(appid)) {
                    alerters = ALERTERS.get(appid);
                } else {
                    alerters = new ArrayList<>();

                    Config.getInstance().getAlerterFactories().parallelStream().forEach(factory -> {
                        IAlerter alerter = factory.create();
                        alerter.setAppId(appid);
                        alerters.add(alerter);
                    });

                    ALERTERS.put(appid, alerters);
                }
            }

            alerters.parallelStream().forEach(iAlerter -> iAlerter.alert(info));

            return null;
        });

        return null;
    }

    @Path("/alert/{appid}/done")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response done(@PathParam ("appid") String appid) {
        if (ALERTERS.containsKey(appid)) {
            final var alerters = ALERTERS.get(appid);

            CompletableFuture.supplyAsync(() -> {
                alerters.parallelStream().forEach(IAlerter::done);
                return null;
            });

            ALERTERS.remove(appid);
        }

        return null;
    }

    @Path("/alert/shutdown")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response shutdown() {
        new Thread(() -> {
            try {
                Thread.sleep(2000);
                System.exit(0);
            } catch (Throwable t) {
                // DO NOTHING
            }
        }).start();

        return null;
    }
}
