/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.impl.alerters;

import me.yoram.jalerter.server.api.IAlerter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 * @author theyoz
 * @since 12/08/18
 */
public class SmtpAlerter extends AbstractSimpleAlerter {
    private static final Logger LOG = LogManager.getLogger(SmtpAlerter.class);
    private static final Random RND = new SecureRandom();

    private String host;
    private int port = -1;
    private String username;
    private String password;
    private String from;
    private String recipients;
    private Map<String, String> options;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(final String recipients) {
        this.recipients = recipients;
    }

    public Map<String, String> getOptions() {
        return options;
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    @Override
    public void validate() throws Exception {
        if (host == null) {
            throw new Exception("host is undefined");
        }

        if (port == -1) {
            throw new Exception("port is undefined");
        }

        if (from == null) {
            throw new Exception("from is undefined");
        }

        if (recipients == null) {
            throw new Exception("recipients is undefined");
        }
    }

    @Override
    public IAlerter create() {
        var res = new SmtpAlerter();
        res.host = host;
        res.port = port;
        res.username = username;
        res.password = password;
        res.from = from;
        res.recipients = recipients;
        res.options = options == null ? null : new HashMap<>(options);

        return res;
    }

    @Override
    public void done() {
        try {
            final var props = new Properties();

            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", port);

            if (options != null) {
                props.putAll(options);
            }

            final Session session;

            if (username != null && password != null) {
                props.put("mail.smtp.auth", "true");

                session = Session.getInstance(props, new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
            } else {
                session = Session.getDefaultInstance(props);
            }

            MimeMessage msg = new MimeMessage(session);
            InternetAddress[] address = InternetAddress.parse(recipients, true);
            msg.setFrom(from);
            msg.setRecipients(Message.RecipientType.TO, address);

            String timeStamp = new SimpleDateFormat("yyyymmdd_hh-mm-ss").format(new Date());
            msg.setSubject("Alert at " + timeStamp);
            msg.setSentDate(new Date());

            final var multipart = new MimeMultipart();

            final StringBuilder sb = new StringBuilder();

            for (var info: getInfos()) {
                sb.append("<p>");

                sb.append("<h2>").append(info.getUid()).append("</h2>");

                sb
                        .append("<div>")
                        .append(info.getDescription() == null ? "" : info.getDescription().replace("\n", "<br>"))
                        .append("</div>");

                if (info.getMime() == null || info.getMime().equals("text/plain")) {
                    sb
                            .append("<div>")
                            .append(info.getData() == null ? "" : info.getData().replace("\n", "<br>"))
                            .append("</div>");
                } else {
                    final var messageBodyPart = new MimeBodyPart();
                    final var file = String.format("file_%s_%d_%s", info.getUid().replace(" ", "_"), RND.nextInt(), ".txt");
                    final var source = new ByteArrayDataSource(Base64.getDecoder().decode(info.getData().getBytes()), info.getMime());

                    messageBodyPart.setDataHandler(new DataHandler(source));
                    messageBodyPart.setFileName(file);
                    multipart.addBodyPart(messageBodyPart);

                    sb.append("<div>Content in attached file: ").append(file).append("</div>");
                }

                sb.append("</p>").append("<hr>");
            }

            msg.setContent(sb.toString(), "text/html");

            if (multipart.getCount() > 0) {
                final var htmlBodyPart = new MimeBodyPart();
                htmlBodyPart.setContent(sb.toString(), "text/html");
                multipart.addBodyPart(htmlBodyPart);

                msg.setContent(multipart);
            }

            Transport.send(msg);
        } catch (Throwable t) {
            LOG.error(t.getMessage(), t);
        }
    }
}
