/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server;

import me.yoram.jalerter.server.utils.AlerterServiceClient;

/**
 * @author theyoz
 * @since 13/08/18
 */
public class Shutdown {
    public static void main(String[] args)  {
        try {
            AlerterServiceClient.shutdown();
        } catch (Throwable t) {
            t.printStackTrace();

            System.exit(1);
        }
    }
}
