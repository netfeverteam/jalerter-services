/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.impl.alerters;

import me.yoram.jalerter.server.api.IAlerter;
import me.yoram.jalerter.server.api.IAlerterFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author theyoz
 * @since 12/08/18
 */
public abstract class AbstractAlerter implements IAlerter, IAlerterFactory {
    private static final Logger LOG = LogManager.getLogger(AbstractAlerter.class);

    String appId;

    @Override
    public void setAppId(final String appId) {
        this.appId = appId;
    }

    @Override
    public IAlerter create() {
        try {
            return this.getClass().getDeclaredConstructor().newInstance();
        } catch (Throwable t) {
            LOG.error(t.getMessage(), t);

            return null;
        }
    }
}
