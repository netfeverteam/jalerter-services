/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.api;
import com.google.gson.Gson;

/**
 * @author theyoz
 * @since 10/08/18
 */
public class AlertInfo {
    private String uid;
    private String description;
    private String data;
    private String mime;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public AlertInfo uid(String uid) {
        setUid(uid);

        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public AlertInfo description(final String description) {
        setDescription(description);

        return this;
    }

    public String getData() {
        return data;
    }

    public void setData(final String data) {
        this.data = data;
    }

    public AlertInfo data(final String data) {
        setData(data);

        return this;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public AlertInfo mime(String mime) {
        setMime(mime);

        return this;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
