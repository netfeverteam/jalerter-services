/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.utils;

import me.yoram.jalerter.server.Main;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.InetAddress;

/**
 * @author theyoz
 * @since 13/08/18
 */
public class Utils {
    private static final String ENV_ALERTER_HOME = "ALERTER_HOME";

    public static File getAlerterHome() {
        var override = System.getProperty(ENV_ALERTER_HOME);

        if (override == null) {
            override = System.getenv(ENV_ALERTER_HOME);
        }

        if (override == null) {
            try {
                override = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())
                        .getParentFile()
                        .getParent();
            } catch (Throwable t) {
                override = System.getProperty("user.dir");
            }
        }

        final var res = new File(override);

        if (!res.exists()) {
            throw new IllegalArgumentException(
                    String.format("The ALERTER_HOME points to %s and it does not exists",
                            res.getAbsolutePath()));
        }

        if (!res.isDirectory()) {
            throw new IllegalArgumentException(String.format("Home folder (%s) is not valid", res.getAbsolutePath()));
        }

        return res;
    }

    public static File getLog4j2File(final File alerterHome) {
        var file = System.getProperty("log4j.configurationFile");

        if (file == null) {
            file = new File(alerterHome, "conf/log4j2.xml").getAbsolutePath();
        }

        var f = new File(file);

        return f.exists() && f.isFile() ? f : null;
    }

    public static File getConfigFile(final File alerterHome) {
        File f = null;
        String hostname;

        try {
            hostname = InetAddress.getLocalHost().getHostName();
        } catch (Throwable t) {
            throw new IllegalArgumentException(String.format("Error getting hostname. %s", t.getMessage()), t);
        }

        if (hostname != null) {
            f = new File(alerterHome, String.format("conf/%s/config.xml", hostname));
        }

        if (f == null || !f.isFile()) {
            f = new File(alerterHome, "conf/config.xml");
        }

        return f.exists() && f.isFile() ? f : null;
    }

    public static int getServiceTcpPort()
            throws SAXException, ParserConfigurationException, IOException, XPathExpressionException {
        final var configFile = Utils.getConfigFile(Utils.getAlerterHome());

        if (configFile == null) {
            throw new IllegalArgumentException("Config not found in conf/config.xml");
        }

        try (final var in = new FileInputStream(configFile)) {
            final var builderFactory = DocumentBuilderFactory.newInstance();
            final var builder = builderFactory.newDocumentBuilder();
            final var xmlDocument = builder.parse(in);
            var xPath = XPathFactory.newInstance().newXPath();
            String expression = "/beans/bean/property[@name='serviceTcpPort']/@value";
            final var node = (Node) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODE);

            return (node != null && node.getTextContent() != null) ?
                    Integer.parseInt(node.getTextContent()) :
                    8989;
        }
    }

    public static void sleep(final long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            // DO NOTHING
        }
    }

    public static String toString(final InputStream in) {
        final var res = new ByteArrayOutputStream();

        try {
            int nread;
            final var buf = new byte[1024];

            while ((nread = in.read(buf)) != -1) {
                res.write(buf, 0, nread);
            }
        } catch (Throwable t) {
            // do nothing
        }

        return new String(res.toByteArray());
    }
}
