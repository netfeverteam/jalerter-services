/*
 * Copyright 2018 Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.yoram.jalerter.server.utils;

import me.yoram.jalerter.server.api.AlertInfo;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Yoram Halberstam (yoram dot halberstam at gmail dot com)
 * @since 13/08/18
 */
public class AlerterServiceClient {
    private static int port;

    static {
        try {
            port = Utils.getServiceTcpPort();
        } catch (Throwable t) {
            System.err.println("Error finding TCP Port, using default 8989");
            port = 8989;
        }
    }

    private static URL getUrl(final String suffix) throws MalformedURLException {
        return new URL(String.format("http://localhost:%d/alert%s", port, suffix));
    }

    public static void add(final String appid, final AlertInfo info) {
        try {
            final var conn = (HttpURLConnection)getUrl(String.format("/%s/add", appid)).openConnection();

            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");

            conn.addRequestProperty("Content-Type", "application/json");
            conn.addRequestProperty("Content-Length", "" + info.toString().getBytes().length);

            try (final var out = conn.getOutputStream()) {
                out.write(info.toString().getBytes());
            }

            conn.getInputStream().close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void done(final String appid) {
        try {
            getUrl(String.format("/%s/done", appid)).openConnection().getInputStream().close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void shutdown() {
        try {
            getUrl("/shutdown").openConnection().getInputStream().close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
