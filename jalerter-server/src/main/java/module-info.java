module me.yoram.jalerter.server {
    exports me.yoram.jalerter.server;
    exports me.yoram.jalerter.server.api;
    exports me.yoram.jalerter.server.impl;
    exports me.yoram.jalerter.server.impl.alerters;

    requires grizzly.http.server;

    requires gson;

    requires java.mail;
    requires java.sql;
    requires java.ws.rs;
    requires java.xml.bind;
    requires java.activation;

    requires jersey.common;
    requires jersey.container.grizzly2.http;
    requires jersey.media.json.jackson;
    requires jersey.server;

    requires org.apache.logging.log4j;
    requires org.apache.logging.log4j.core;

    requires spring.beans;
    requires spring.context;
}