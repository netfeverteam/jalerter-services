#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
PARENT_DIR=$(dirname "$DIR")

$DIR/setenv.sh

ALERTER_HOME=${ALERTER_HOME:-$PARENT_DIR}

$JAVA_HOME/bin/java -jar $ALERTER_HOME/lib/jalerter-server.jar -DALERTER_HOME=$ALERTER_HOME

